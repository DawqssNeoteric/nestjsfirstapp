import { Injectable } from '@nestjs/common';
import { Item } from './item.interface';

@Injectable()
export class ItemsService {
  private readonly items: Item[] = [
    {name: 'Marijuana', price: 50},
    {name: 'Pizza', price: 29},
    {name: 'Honor', price: null},
    {name: 'Burgier', price: 20},
  ];

  findAll(): Item[] {
    return this.items;
  }

  create(item: Item) {
    this.items.push(item);
  }
}